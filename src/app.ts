import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import { router as math } from './routes/math'
import { router as maps } from './routes/maps'
import { router as line } from './routes/line'

const app = express()
const port = 5000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/math/', math)
app.use('/maps/', maps)
app.use('/line/', line)

app.listen(process.env.PORT || port, () => {
  console.log(`app listening at http://localhost:${port}`)
})