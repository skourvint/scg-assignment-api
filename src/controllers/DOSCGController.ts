import * as request from 'request'
import { Client as client } from '@googlemaps/google-maps-services-js'
import * as redis from 'redis'
// import * as ws from 'ws'

// const redisClient = redis.createClient()
const redisClient = redis.createClient("redis://:p449b02cf64fdf7bec756cdafd956abfce9b11b6b07ad5cd1ff64efa924e5ad5f@ec2-3-82-177-133.compute-1.amazonaws.com:7589")

export function findNumber(req, res) {
  try {
    const arr = JSON.parse(req.body.qa)
    const numberRedisKey = "math:"+arr

    redisClient.get(numberRedisKey, (err, data) => {
      if (data) {
        console.log('redis')
        return res.send(JSON.parse(data))
      } else {
        console.log('not redis')
        let findIndexAt = []
        let indexAns = {}

        // create new array to collect index of input array that is not an integer
        arr.every((val, indexAt) => {
          if(!Number.isInteger(arr[indexAt])) {
            findIndexAt.push(indexAt)
          }
          return true
        })
        
        // findIndexAt = [0, 1, 6]
        while (findIndexAt.length > 0) {
          // find answer
          findIndexAt.forEach(indexAt => {
            // if last index is more than array length then finished
            if((indexAt + 1) > arr.length) return

            // find out next integer
            // if next index of array is not integer just skip
            // then will in the loop if next index is integer
            if(Number.isInteger(arr[indexAt + 1])) {
              let ans = arr[indexAt + 1] - (2 * indexAt)
              indexAns[arr[indexAt]] = ans
              arr[indexAt] = ans
              return
            }

            // find out previous integer
            if(Number.isInteger(arr[indexAt - 1])) {
              let ans = arr[indexAt - 1] + (2 * (indexAt -1))
              indexAns[arr[indexAt]] = ans
              arr[indexAt] = ans
              return
            }
          })

          // remove findIndexAt if it can be solved
          findIndexAt = findIndexAt.filter((val, indexAt) => !Number.isInteger(arr[indexAt]))
        }
        redisClient.setex(numberRedisKey, 60, JSON.stringify(indexAns))
        res.send(indexAns)
      }
    })
  } catch (err) {
    console.error(err)
    res.send({ message: err.message }, 400)
  }
}

export function findValue(req, res) {
  try {
    const valA = req.params.number
    const valueRedisKey = "math:"+valA

    redisClient.get(valueRedisKey, (err, data) => {
      if (data) {
        console.log('redis')
        return res.send(JSON.parse(data))
      } else {
        console.log('not redis')
        let dataSet = []
    
        // as we know the answer of B and C
        const answerB = 23
        const answerC = -21

        const valB = answerB - valA
        const valC = answerC - valA

        dataSet.push({
          valueB: valB,
          valueC: valC
        })

        let dataSetObj = Object.assign({}, ...dataSet)

        redisClient.setex(valueRedisKey, 60, JSON.stringify(dataSetObj))
        res.send(dataSetObj)
      }
    })
  } catch (err) {
    console.error(err)
    res.send({ message: err.message }, 400)
  }
}
export function googleMaps(req, res) {
  try {
    const gmapsRedisKey = "math:googlemaps"

    redisClient.get(gmapsRedisKey, (err, data) => {
      if (data) {
        // console.log('redis')
        return res.send(JSON.parse(data))
      } else {
        // console.log('not redis')
        const apiKey = "AIzaSyDFHaVJUyQ59W_bwVOkhOJmVmhqOYW9OEc"

        client.prototype.directions({
          params: {
            origin: 'SCG+สำนักงานใหญ่+บางซื่อ',
            destination: 'เซ็นทรัลเวิลด์',
            key: apiKey,
          },
          timeout: 1000, // milliseconds
        })
        .then((r) => {
          // console.log(r.data)
          redisClient.setex(gmapsRedisKey, 60, JSON.stringify(r.data))
          res.send(r.data)
        })
        .catch((e) => {
          console.log(e.response.data.error_message)
          res.sendStatus(404)
        })
      }
    })
  } catch (err) {
    console.error(err)
    res.send({ message: err.message }, 400)
  }
}

export function lineBot(req, res) {
  try {
    let replyToken = req.body.events[0].replyToken
    let msg = req.body.events[0].message.text

    const helloTextArr = ['ดีจ้า', 'สวัสดี']
    const catText = 'แมว'
    const catURL  = 'https://i.pinimg.com/originals/cc/22/d1/cc22d10d9096e70fe3dbe3be2630182b.jpg'
    
    let data = []
    if (helloTextArr.indexOf(msg) > -1) {
      data = [
        {
          type: 'text',
          text: 'น้องอันโต สวัสดีฮับ'
        }
      ]

      lineReply(replyToken, data)
    } else if (msg.includes(catText)) {
      data = [
        {
          type: 'image',
          originalContentUrl: catURL,
          previewImageUrl: catURL
        }
      ]

      lineReply(replyToken, data)
    }
    else {
      data = [
        {
          type: 'text',
          text: 'คำถามนี้น้องอันโตไม่สามารถตอบได้ ต้องขออภัยและจะนำไปปรับปรุงฮับ'
        }
      ]
      lineReply(replyToken, data)

      setTimeout(() => {
        lineNotification(msg)
      }, 10000)
    }

    res.sendStatus(200)
  } catch (err) {
    console.error(err)
    res.send({ message: err.message }, 400)
  }
}

function lineReply(replyToken, data) {
  const accessToken = "jTz+JNnBYwziz5noam0A7FvTnFjc0OTHYsXdKBrVccWEeeUsUSxkW7wx4nL5YjiDvR3XRjWnGB6m81GITzTIO0Ogs/dmtA6BL9jvUfQZNzD9sJTr5mCFrEODvLu26EBrzVbC5CRMIrixJ+rQsr34+QdB04t89/1O/w1cDnyilFU="

  let headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${accessToken}` 
  }

  let body = JSON.stringify({
    replyToken: replyToken,
    messages: data
  })

  request.post({
    url: 'https://api.line.me/v2/bot/message/reply',
    headers: headers,
    body: body
  }, (err, res, body) => {
      console.log('status = ' + res.statusCode)
  })
}

function lineNotification(msg) {
  console.log('lineNotification')
}
