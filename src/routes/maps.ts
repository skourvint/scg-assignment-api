import * as express from 'express'
import * as controller from '../controllers/DOSCGController'

export const router = express.Router()
router.get('/direction', controller.googleMaps)
